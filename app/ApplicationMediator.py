# Standard Library
import os
import re
import sys

# User-Defined Classes
import Mediator
import MenuPart
import InstallationPart
import ScriptPart

class ApplicationMediator(Mediator.AbstractMediator):
	
	"""
	This class implements the notification

	"""

	def __init__(self) -> None:
		"""It makes the compositions of other application parts"""
		
		# creating the compositions
		self.Menu = MenuPart.Menu(self)
		self.Intall = InstallationPart.Installing(self)
		self.UpdateMO = InstallationPart.MOUpdate(self)
		self.UpdateXmrig = InstallationPart.LPUpdate(self)
		self.UpdateLP = InstallationPart.XmrigUpdate(self)
		self.UpdateApp = InstallationPart.ApplicationUpdate(self)
		self.CreateScript = ScriptPart.CreateScript(self)
		self.RunScript = ScriptPart.RunScript(self)
		self.DeleteScript = ScriptPart.DeleteScript(self)
		self.UpdateAll = InstallationPart.UpdateAll(self)

		#checking for a wild pointer
		assert self.Menu != None
		assert self.Intall != None
		assert self.UpdateMO != None
		assert self.UpdateXmrig != None 
		assert self.UpdateLP != None 
		assert self.UpdateApp != None
		assert self.CreateScript != None
		assert self.DeleteScript != None
		assert self.RunScript != None
		assert self.UpdateAll != None
		

	def notify(self, event) -> None:
		"""It runs parts of the application based on the notification"""

		try:
			if event == "Menu":
				self.Menu.Run()
			elif event == "Intall":
				self.Intall.Run()
			elif event == "UpdateMO":
				self.UpdateMO.Run()
			elif event == "UpdateXmrig":
				self.UpdateXmrig.Run()
			elif event == "UpdateLP":
				self.UpdateLP.Run()
			elif event == "UpdateApp":
				self.UpdateApp.Run()
			elif event == "CreateScript":
				self.CreateScript.Run()
			elif event == "DeleteScript":
				self.DeleteScript.Run()
			elif event == "RunScript":
				self.RunScript.Run()
			elif event == "UpdateAllMiners":
				self.UpdateAll.Run()
			else:
				raise ValueError("Unsupported Notification")
		except ValueError as error:
			exc_type, exc_obj, exc_tb = sys.exc_info()
			fname = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]
			print(exc_type, fname, exc_tb.tb_lineno)
			sys.exit(error)
			
if __name__ == "__main__":
	
	print("\n///------------------------------------------------------///")
	print("///------------Running the Python Application------------///")
	print("///------------------------------------------------------///\n")

	application = ApplicationMediator()
	application.notify("Intall")
	
	print("\n///-------------End of the Python Application------------///")
