# Standard Library
import os
import re
import time

# User-Defined Classes
import Dictionary
import Singleton
import ChainOfResponsibility
import DoubleCheck

class GetWallet(ChainOfResponsibility.AbstractHandler):
	
	"""
    This class is getting and storing wallet address
    in the singleton to be used in another handler
    
    """
    
	def __init__(self):
		"""it gets the singleton instance"""
		
		self.instance = Singleton.Singleton()
				
	def Handle(self):
		"""it gets the wallet address
		
		:returns: the next handler on the chain
		:rtype Handler
		
		"""
		
		# read the wallet
		with open(Dictionary.Commands.get("mywallet")) as f:
			self.instance.wallet = f.readline()
		self.instance.wallet = re.sub('\n', '', self.instance.wallet)
		print(self.instance.wallet)
		print("\n\n\n\n")
		
		readyInput = ""
		exitLoop = False
		
		# make sure if it is correct wallet and store it
		while exitLoop == False:
			readyInput = input("Is this correct wallet address? (Y)es/(N)o: ")
			time.sleep(self.ONE_SEC_DELAY)
			
			if readyInput.lower() == "yes" or readyInput.lower() == "y":
				exitLoop = True
			
			elif readyInput.lower() == "no" or readyInput.lower() == "n":
				self.instance.wallet = input("copy and paste it here: ")
				time.sleep(self.ONE_SEC_DELAY)
				with open(Dictionary.Commands.get("mywallet"), 'w') as f:
					f.write(self.instance.wallet)
					self.instance.wallet = re.sub('\n', '', self.instance.wallet)
			else:
				print("There is no such option")
		
		# go to next handler		
		return super().Handle()


class GetInitialCoresAndRam(ChainOfResponsibility.AbstractHandler):

	"""
	This class is get initial information about number of cores
	and amount of ram

	"""

	def __init__(self):
		"""it gets the singleton instance"""
		
		self.instance = Singleton.Singleton()

	def Handle(self):
		"""it get initial information 
		
		:returns: the next handler on the chain
		:rtype Handler
		
		"""

		# get cores	
		os.system("nproc --all >> cores.txt")
		with open("cores.txt", "r") as f:
			self.instance.maxcores = f.readline()
		os.system("rm cores.txt")
		
		# get ram
		os.system("free -g | grep Mem: | awk '{print $2}' >> mem.txt")
		with open("mem.txt", "r") as f:
			self.instance.mem = f.readline()
		os.system("rm mem.txt")
		
		# go to next handler
		return super().Handle()

		
class GetMinerAndAlgo(ChainOfResponsibility.AbstractHandler):

	"""
	This class gets miner and algo
	and stores them in the singleton 

	"""

	MO_OPTION = "1"
	LP_OPTION = "2"
	XMRIG_OPTION = "3"
	BIG_RAM = 7

	def __init__(self):
		"""it gets the singleton instance"""
		
		self.instance = Singleton.Singleton()
		self.exit = False

	def GetAlgoX(self):
		"""it gets the algo with randomx if xmrigx was choosen
		
		:returns: the next handler on the chain
		:rtype Handler
		
		"""
		
		# prints all possible algos
		print("\n\n\n\n")
		print("\n++++++++++++++++++++\n")
		print("Choose the Algo")
		print("\n++++++++++++++++++++\n")	
		for i in range(len(Dictionary.AlgosX)):
			print("(" + str(i + 1) + ")" + Dictionary.AlgosX[i])
		print("\n--------------------\n")
		print("(0)exit\n\n")
		
		readyInput = ""
		exitLoop = False
		
		# waits for the chosen algo
		while exitLoop == False:
			readyInput = input("Choose the Algo: ")
			time.sleep(self.ONE_SEC_DELAY)
			
			if readyInput == self.EXIT_OPTION:
				option = DoubleCheck.AreYouSure()
				if option == True:
					self.exit = True
					exitLoop = True
			
			elif int(readyInput) > int(self.EXIT_OPTION) and (int(readyInput) - 1) < len(Dictionary.AlgosX):
				option = DoubleCheck.AreYouSure()
				if option == True:
					self.instance.algo = Dictionary.AlgosX[int(readyInput) - 1]
					exitLoop = True
			else:
				print("There is no such option")

	def GetAlgo(self):
		"""it gets the algo if xmrig was choosen
		
		:returns: the next handler on the chain
		:rtype Handler
		
		"""
		
		# prints all possible algos
		print("\n\n\n\n")
		print("\n++++++++++++++++++++\n")
		print("Choose the Algo")
		print("\n++++++++++++++++++++\n")	
		for i in range(len(Dictionary.Algos)):
			print("(" + str(i + 1) + ")" + Dictionary.Algos[i])
		print("\n--------------------\n")
		print("(0)exit\n\n")
		
		readyInput = ""
		exitLoop = False
		
		# waits for the chosen algo
		while exitLoop == False:
			readyInput = input("Choose the Algo: ")
			time.sleep(self.ONE_SEC_DELAY)
			
			if readyInput == self.EXIT_OPTION:
				option = DoubleCheck.AreYouSure()
				if option == True:
					self.exit = True
					exitLoop = True
			
			elif int(readyInput) > int(self.EXIT_OPTION) and (int(readyInput) - 1) < len(Dictionary.Algos):
				option = DoubleCheck.AreYouSure()
				if option == True:
					self.instance.algo = Dictionary.Algos[int(readyInput) - 1]
					exitLoop = True
			else:
				print("There is no such option")

	def GetMiner(self):
		"""it gets the miner
		
		:returns: the next handler on the chain
		:rtype Handler
		
		"""
		
		# prints miners
		print("\n\n\n\n")
		print("\n++++++++++++++++++++\n")
		print("Choose the Miner")
		print("\n++++++++++++++++++++\n")
		print("(1)MO")
		print("(2)LP")
		print("(3)Xmrig")
		print("\n--------------------\n")
		print("(0)exit\n\n")
		
		readyInput = ""
		exitLoop = False
		
		# waits for the chosen miner
		while exitLoop == False:
			readyInput = input("Choose the Miner: ")
			time.sleep(self.ONE_SEC_DELAY)
			
			if readyInput == self.EXIT_OPTION:
				option = DoubleCheck.AreYouSure()
				if option == True:
					self.exit = True
					exitLoop = True
			
			elif readyInput == self.MO_OPTION:
				option = DoubleCheck.AreYouSure()
				if option == True:
					self.instance.miner = Dictionary.Commands.get("run_mo")
					exitLoop = True
						
			elif readyInput == self.LP_OPTION:
				option = DoubleCheck.AreYouSure()
				if option == True:
					self.instance.miner = Dictionary.Commands.get("run_lp")
					exitLoop = True
				
			elif readyInput == self.XMRIG_OPTION:
				option = DoubleCheck.AreYouSure()
				if option == True:
					self.instance.miner = Dictionary.Commands.get("run_xmrig")
					if int(self.instance.mem) > self.BIG_RAM:
						self.GetAlgoX()
					else:
						self.GetAlgo()
					exitLoop = True
			else:
				print("There is no such option")
				
		if self.exit == True:
			# exit the chain
			return
		else:
			# go to next handler
			return super().Handle()
		
						
	def Handle(self):
		"""it runs the GetMiner and GetAlgo methods
		
		:returns: the next handler on the chain
		:rtype Handler
		
		"""
		# runs the methods
		self.GetMiner()

		
class GetPool(ChainOfResponsibility.AbstractHandler):
	
	"""
	This class gets pool
	and stores it in the singleton 

	"""

	MO_POOL_OPTION = "1"
	LP_POOL_OPTION = "2"

	def __init__(self):
		"""it gets the singleton instance"""

		self.instance = Singleton.Singleton()
		self.exit = False
				
	def Handle(self):
		"""it gets the pool
		
		:returns: the next handler on the chain
		:rtype Handler
		
		"""
		if self.instance.miner == Dictionary.Commands.get("run_xmrig"):
			# prints pools
			print("\n++++++++++++++++++++\n")
			print("Choose the Pool")
			print("\n++++++++++++++++++++\n")
			print("(1)MoneroOcean")
			print("(2)LibertyPool")
			print("\n--------------------\n")
			print("(0)exit\n\n")
			
			readyInput = ""
			exitLoop = False
			
			# waits for the chosen pool
			while exitLoop == False:
				readyInput = input("Choose the Pool: ")
				time.sleep(self.ONE_SEC_DELAY)
				
				if readyInput == self.EXIT_OPTION:
					option = DoubleCheck.AreYouSure()
					if option == True:
						self.exit == True
						exitLoop = True
						
				elif readyInput == self.MO_POOL_OPTION:
					option = DoubleCheck.AreYouSure()
					if option == True:
						self.instance.pool = Dictionary.Commands.get("mo_pool")
						exitLoop = True
					
				elif readyInput == self.LP_POOL_OPTION:
					option = DoubleCheck.AreYouSure()
					if option == True:
						self.instance.pool = Dictionary.Commands.get("lp_pool")
						exitLoop = True
									
				else:
					print("There is no such option")
		
		elif self.instance.miner == Dictionary.Commands.get("run_mo"):
			self.instance.pool = Dictionary.Commands.get("mo_pool")
			
		elif self.instance.miner == Dictionary.Commands.get("run_lp"):
			self.instance.pool = Dictionary.Commands.get("lp_pool")
		
		if self.exit == True:
			# exit the chain
			return
		else:
			# go to next handler
			return super().Handle()
		
	
class GetCores(ChainOfResponsibility.AbstractHandler):

	"""
	This class get amount of cores 
	and stores it in the singleton 

	"""

	def __init__(self):
		"""it gets the singleton instance"""
		
		self.instance = Singleton.Singleton()
		self.exit = False

	def Handle(self):
		"""it gets the amount of cores
		
		:returns: the next handler on the chain
		:rtype Handler
		
		"""

		# prints core info
		print("\n++++++++++++++++++++\n")
		print("Choosing amount of cores")
		print("\n++++++++++++++++++++\n")
		print("This is your maximum Cores: "+ str(self.instance.maxcores))
		print("Leave one core to run the OS")
		print("\n--------------------\n")
		print("(0)exit\n\n")
		
		readyInput = ""
		exitLoop = False

		# waits for the chosen amount of cores
		while exitLoop == False:
			readyInput = input("Choose amount of cores: ")
			time.sleep(self.ONE_SEC_DELAY)
			
			if readyInput == self.EXIT_OPTION:
				option = DoubleCheck.AreYouSure()
				if option == True:
					self.exit == True
					exitLoop = True

			elif int(readyInput) > int(self.instance.maxcores) - 1:
				print("Too many cores")
			
			elif readyInput.isdigit() == False:
				print("it is not a digit")
			
			else:
				option = DoubleCheck.AreYouSure()
				if option == True:
					self.instance.cores = readyInput
					exitLoop = True
			
		if self.exit == True:
			# exit the chain
			return
		else:
			# go to next handler
			return super().Handle()
		

class CreateScript(ChainOfResponsibility.AbstractHandler):

	"""
	This class creates mining scripts

	"""

	EMPTY_NAME = ""

	def __init__(self):
		"""it get the singleton instance"""
		
		self.instance = Singleton.Singleton()
		self.exit = False

	def ChoosingFileName(self):
		"""it gets file name"""
		
		# prints info
		print("\n++++++++++++++++++++\n")
		print("Naming the minining script")
		print("\n++++++++++++++++++++\n")
		print("(0)exit\n\n")
		
		readyInput = ""
		exitLoop = False
		
		# waits for the chosen name
		while exitLoop == False:
			readyInput = input("Name the minining script without .sh: ")
			time.sleep(self.ONE_SEC_DELAY)
			
			if readyInput == self.EXIT_OPTION:
				option = DoubleCheck.AreYouSure()
				if option == True:
					self.exit = True
					exitLoop = True
					
			elif readyInput == self.EMPTY_NAME:
				print("you must give a name to the mining script")
			
			else:
				option = DoubleCheck.AreYouSure()
				if option == True:
					self.instance.filename = readyInput + ".sh"
					exitLoop = True
					
	def ChoosingDeviceName(self):
		"""it gets device name"""
		
		# prints info
		print("\n++++++++++++++++++++\n")
		print("Naming the mobile device")
		print("\n++++++++++++++++++++\n")
		print("(0)exit\n\n")
		
		readyInput = ""
		exitLoop = False
		
		# waits for the chosen name
		while exitLoop == False:
			readyInput = input("Name the mobile device: ")
			time.sleep(self.ONE_SEC_DELAY)
			
			if readyInput == self.EXIT_OPTION:
				option = DoubleCheck.AreYouSure()
				if option == True:
					self.exit = True
					exitLoop = True
					
			elif readyInput == self.EMPTY_NAME:
				print("you must give a name to the mobile device")
			
			else:
				option = DoubleCheck.AreYouSure()
				if option == True:
					self.instance.name = readyInput
					exitLoop = True
    			
	def Handle(self):
		"""it creates mining script
		
		:returns: the next handler on the chain
		:rtype Handler
		
		"""
		
		# runs the methods
		self.ChoosingFileName()
		if self.exit == False:
			self.ChoosingDeviceName()
		
		if self.exit == True:
			# exit the chain
			return
		else:
			# creates mining script
			if self.instance.miner == Dictionary.Commands.get("run_xmrig"):
				text = "#!/bin/sh\n" + self.instance.miner + " --threads=" + self.instance.cores + " -a " + self.instance.algo + " -k" + self.instance.AFFINITY + self.instance.MEMORYPOOL + \
				" -o " + self.instance.pool + " -u " + self.instance.wallet + " -p " + self.instance.name + "~" + self.instance.algo
			else:
				text = "#!/bin/sh\n" + self.instance.miner + " --threads=" + self.instance.cores + " -k" + self.instance.AFFINITY + self.instance.MEMORYPOOL + \
				" -o " + self.instance.pool + " -u " + self.instance.wallet + " -p " + self.instance.name
			
			with open(self.instance.filename, 'w') as f:
				f.write(text)
			
			# gives permission and moves it to the script folder
			os.system("chmod u+x " + self.instance.filename)
			os.system("mv " + self.instance.filename + " mobile-miner-termux/scripts/" + self.instance.filename)
			
			# go to next handler
			return super().Handle()
		
class GetScripts(ChainOfResponsibility.AbstractHandler):
	
	"""
	This class gets list of mining scripts
	and stores it in the singleton 

	"""

	def __init__(self):
		"""it get the singleton instance"""
		
		self.instance = Singleton.Singleton()
		self.exit = False
				
	def Handle(self):
		"""get list of mining scripts and prints them
		
		:returns: the next handler on the chain
		:rtype Handler
		
		"""
		
		# Get the list of all mining scripts
		self.instance.scriptList = os.listdir("mobile-miner-termux/scripts/")
		
		print("\n++++++++++++++++++++\n")
		print("Mining Scripts")
		print("\n++++++++++++++++++++\n")
		for i in range(len(self.instance.scriptList)):
				print("(" + str(i + 1) + ")" + self.instance.scriptList[i])
		print("(0)exit\n\n")
		print("\n--------------------\n")
		
		# go to next handler
		return super().Handle()
		
		
class RunScript(ChainOfResponsibility.AbstractHandler):
	
	"""
    This class runs a mining script
    
    """
	
	def __init__(self):
		"""it get the singleton instance"""
		
		self.instance = Singleton.Singleton()
		self.exit = False
    			
	def Handle(self):
		"""it runs the mining script
		
		:returns: the next handler on the chain
		:rtype Handler
		
		"""
		
		readyInput = ""
		exitLoop = False
		
		# waits for the chosen name
		while exitLoop == False:
			readyInput = input("Choose the mining script based on the assigned digit: ")
			time.sleep(self.ONE_SEC_DELAY)
			
			if readyInput == self.EXIT_OPTION:
				option = DoubleCheck.AreYouSure()
				if option == True:
					self.exit = True
					exitLoop = True
					
			elif int(readyInput) > int(self.EXIT_OPTION) and (int(readyInput) - 1) < len(self.instance.scriptList):
				option = DoubleCheck.AreYouSure()
				if option == True:
					os.system("sh  mobile-miner-termux/scripts/" + self.instance.scriptList[int(readyInput) - 1])
					exitLoop = True
			else:
				print("There is no such option")
		
		if self.exit == True:
			# exit the chain
			return
		else:
			# go to next handler
			return super().Handle()
		
class DeleteScript(ChainOfResponsibility.AbstractHandler):
	
	"""
	This class deletes a mining script

	"""

	def __init__(self):
		"""it get the singleton instance"""
		
		self.instance = Singleton.Singleton()
		self.exit = False
				
	def Handle(self):
		"""it deletes the mining script
		
		:returns: the next handler on the chain
		:rtype Handler
		
		"""
		
		readyInput = ""
		exitLoop = False
		
		# waits for the chosen name
		while exitLoop == False:
			readyInput = input("Choose the mining script based on the assigned digit: ")
			time.sleep(self.ONE_SEC_DELAY)
			
			if readyInput == self.EXIT_OPTION:
				option = DoubleCheck.AreYouSure()
				if option == True:
					self.exit = True
					exitLoop = True
					
			elif int(readyInput) > int(self.EXIT_OPTION) and (int(readyInput) - 1) < len(self.instance.scriptList):
				option = DoubleCheck.AreYouSure()
				if option == True:
					os.system("rm  mobile-miner-termux/scripts/" + self.instance.scriptList[int(readyInput) - 1])
					exitLoop = True
			else:
				print("There is no such option")
		
		if self.exit == True:
			# exit the chain
			return
		else:
			# go to next handler
			return super().Handle()
