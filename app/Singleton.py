class SingletonMeta(type):

	"""
	This class is the metaclass of singleton.

	"""

	#instance
	staticInstances = {}

	def __call__(cls, *args, **kwargs) -> staticInstances:
		"""It calls the SingletonMeta and checks if it already exits.
		If an instance is already allocated then new singleton will 
		not be created
		
		:param *args: number of arguments
		:param **kwargs: variable-length argument list
		:returns: instance of the SingletonMeta
		:rtype staticInstances
		
		"""
		
		if cls not in cls.staticInstances:
			instance = super().__call__(*args, **kwargs)
			cls.staticInstances[cls] = instance
		return cls.staticInstances[cls]


class Singleton(metaclass = SingletonMeta):
	
	"""
	This class is the singleton which uses 
	SingletonMeta as metaclass.
	It consists of shared constants and variables 
	which are used within script chain

	"""
	# constants
	ASTROBWT = " --astrobwt-avx2 --astrobwt-max-size=600 "
	AFFINITY = " --cpu-affinity=0xFF " 
	MEMORYPOOL = " --cpu-memory-pool=500 "


	# variables
	scriptList = []
	wallet = ""
	pool = ""
	miner = ""
	algo = ""
	maxcores = ""
	ram = ""
	cores = ""
	name = ""
	filename = ""
