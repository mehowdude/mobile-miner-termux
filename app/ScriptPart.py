# Standard Library
import os

# User-Defined Classes
import Mediator
import ScriptHandlers
import Dictionary

class CreateScript(Mediator.ApplicationPart):
	
	"""
	This is the class responsible for creating
	mining scripts

	"""

	def SetHandlers(self) -> None:
		"""It sets up the handlers"""
		
		self.wallet = ScriptHandlers.GetWallet()
		self.initial = ScriptHandlers.GetInitialCoresAndRam()
		self.miner = ScriptHandlers.GetMinerAndAlgo()
		self.pool = ScriptHandlers.GetPool()
		self.cores = ScriptHandlers.GetCores()
		self.script = ScriptHandlers.CreateScript()
		
		#checking for a wild pointer
		assert self.wallet != None
		assert self.initial != None
		assert self.miner != None
		assert self.pool != None
		assert self.cores != None
		assert self.script != None
		
		# creating the chain
		self.wallet.SetNext(self.initial).SetNext(self.miner).SetNext(self.pool).SetNext(self.cores).SetNext(self.script)
		
		
	def Run(self) -> None:
		"""It runs the chain of responsibility"""
		
		# setting up the chain
		self.SetHandlers()
		
		# run the chain
		self.wallet.Handle()
		
		# return to the menu
		self.mediator.notify("Menu")


class RunScript(Mediator.ApplicationPart):
	
	"""
	This is the class responsible for running
	mining scripts

	"""

	def SetHandlers(self) -> None:
		"""It sets up the handlers"""
		
		self.get = ScriptHandlers.GetScripts()
		self.run = ScriptHandlers.RunScript()
		
		#checking for a wild pointer
		assert self.get != None
		assert self.run != None
		
		# creating the chain
		self.get.SetNext(self.run)

	def Run(self) -> None:
		"""It runs the chain of responsibility"""
		
		# setting up the chain
		self.SetHandlers()
		
		# run the chain
		self.get.Handle()
		
		# return to the menu
		self.mediator.notify("Menu")
 
        
class DeleteScript(Mediator.ApplicationPart):

	"""
	This is the class responsible for deleting
	mining scripts

	"""

	def SetHandlers(self) -> None:
		"""It sets up the handlers"""
		
		self.get = ScriptHandlers.GetScripts()
		self.delete = ScriptHandlers.DeleteScript()
		
		#checking for a wild pointer
		assert self.get != None
		assert self.delete != None
		
		# creating the chain
		self.get.SetNext(self.delete)

	def Run(self) -> None:
		"""It runs the chain of responsibility"""
		
		# setting up the chain
		self.SetHandlers()
		
		# run the chain
		self.get.Handle()
		
		# return to the menu
		self.mediator.notify("Menu")

