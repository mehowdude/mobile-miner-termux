class AbstractMediator():

	"""
	The purpose of this class is to create a virtual 
	notification to make parts of the application to 
	communicate with each other

	"""
		
	def notify(self, event) -> None:
		pass





class ApplicationPart():

	"""
	This is the base class which is inherited by parts of the application
	responsible for updating, installation, creation of mining scripts 

	"""

	ONE_SEC_DELAY = 1
	EXIT_OPTION = "0"

	def __init__(self, mediator) -> None:
		"""It is the constructor of the ApplicationPart,
		it gets the instance of the mediator
		
		:param AbstractMediator mediator: it is the instance of AbstractMediator
		
		"""
		self.mediator = mediator
		
		#checking for a wild pointer
		assert self.mediator != None
        
