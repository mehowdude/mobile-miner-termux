# Standard Library
import os

# User-Defined Classes
import Mediator
import InstallationHandlers
import Dictionary

class Installing(Mediator.ApplicationPart):

	"""
	This is the class responsible for installing
	the application

	"""

	def SetHandlers(self) -> None:
		"""It sets up the handlers"""
		
		# setting up the chain
		self.installation = InstallationHandlers.InstallationHandler()
		self.introduction = InstallationHandlers.IntroductionHandler()
		self.manual = InstallationHandlers.ManualHandler()
		self.MO = InstallationHandlers.MOInstalliationHandler()
		self.Xmrig = InstallationHandlers.XmrigInstalliationHandler()
		self.LP = InstallationHandlers.LPInstalliationHandler()
		
		#checking for a wild pointer
		assert self.installation != None
		assert self.introduction != None
		assert self.manual != None
		assert self.MO != None
		assert self.Xmrig != None
		assert self.LP != None
		
		# creating the chain
		self.installation.SetNext(self.introduction).SetNext(self.manual).SetNext(self.MO).SetNext(self.Xmrig).SetNext(self.LP)
		
		
	def Run(self) -> None:
		"""It runs the chain of responsibility"""
		
		# setting up the chain
		self.SetHandlers()
		
		# run the chain
		self.installation.Handle()
		
		# return to the menu
		self.mediator.notify("Menu")
		
class UpdateAll(Mediator.ApplicationPart):

	"""
	This is the class responsible for updating
	all miners

	"""

	def SetHandlers(self) -> None:
		"""It sets up the handlers"""
		
		# setting up the chain
		self.MO = InstallationHandlers.MOInstalliationHandler()
		self.Xmrig = InstallationHandlers.XmrigInstalliationHandler()
		self.LP = InstallationHandlers.LPInstalliationHandler()
		
		#checking for a wild pointer
		assert self.MO != None
		assert self.Xmrig != None
		assert self.LP != None
		
		# creating the chain
		self.MO.SetNext(self.Xmrig).SetNext(self.LP)
		
		
	def Run(self) -> None:
		"""It runs the chain of responsibility"""
		
		# setting up the chain
		self.SetHandlers()
		
		# run the chain
		self.MO.Handle()
		
		# return to the menu
		self.mediator.notify("Menu")


class MOUpdate(Mediator.ApplicationPart):

	"""
	This is the class responsible for updating
	the MoneroOcean miner

	"""

	def Run(self) -> None:
		"""It runs the chain of responsibility"""
		
		# setting up the chain
		MO = InstallationHandlers.MOInstalliationHandler()
		
		#checking for a wild pointer
		assert MO != None
		
		#run the chain
		MO.Handle()
		
		# deleting a copy of xmrig which is automatically invoked 
		os.system(Dictionary.Commands.get("delete_xmrig"))
		
		# return to the menu
		self.mediator.notify("Menu")
 
        
class LPUpdate(Mediator.ApplicationPart):

	"""
	This is the class responsible for updating
	the Liberty-Pool miner

	"""

	def Run(self) -> None:
		"""It runs the chain of responsibility"""
		
		# setting up the chain
		LP = InstallationHandlers.LPInstalliationHandler()
		
		#checking for a wild pointer
		assert LP != None
		
		#run the chain
		LP.Handle()
		
		# return to the menu
		self.mediator.notify("Menu")
 
        
class XmrigUpdate(Mediator.ApplicationPart):

	"""
	This is the class responsible for updating
	the Xmrig miner

	"""

	def Run(self) -> None:
		"""It runs the chain of responsibility"""
		
		# setting up the chain
		Xmrig = InstallationHandlers.XmrigInstalliationHandler()
		
		#checking for a wild pointer
		assert Xmrig != None
		
		#run the chain
		Xmrig.Handle()
		
		# return to the menu
		self.mediator.notify("Menu")
  
        
class ApplicationUpdate(Mediator.ApplicationPart):

	"""
	This is the class responsible for updating
	the application

	"""

	def Run(self) -> None:
		"""It runs the updater script"""
		
		# rename the update script
		os.system(Dictionary.Commands.get("update_1"))
		
		# run the update script
		os.system(Dictionary.Commands.get("update_2"))
		
		exit()
