# This Dictionary converts nicknames to commands

Commands = {
    "mywallet" : "mobile-miner-termux/info/mywallet.txt",
    
    "make_info_dir" : "mkdir mobile-miner-termux/info",
    "make_script_dir" : "mkdir mobile-miner-termux/scripts",
    "make_download_dir" : "mkdir mobile-miner-termux/download",
    
    "make_mo_dir" : "mkdir mobile-miner-termux/download/mo",
    "make_lp_dir" : "mkdir mobile-miner-termux/download/lp",
    "make_xmrig_dir" : "mkdir mobile-miner-termux/download/xmrig",
    
    "make_rigs_dir" : "mkdir mobile-miner-termux/rigs",
    
    "update_1" : "mv mobile-miner-termux/updater/updater.sh mobile-miner-termux/updater/old_updater.sh",
    "update_2" : "mobile-miner-termux/updater/old_updater.sh",
    
    "install_xmrig" : "mobile-miner-termux/download/xmrig/xmrig",
    
    "mo_pool" : "gulf.moneroocean.stream:10001",
    "lp_pool" : "mine.liberty-pool.com:1111",

    "run_mo" : "mobile-miner-termux/rigs/mo",
    "run_lp" : "mobile-miner-termux/rigs/lp",
    "run_xmrig" : "mobile-miner-termux/rigs/xmrig",
    
    "download_mo" : "git clone https://github.com/MoneroOcean/xmrig.git",
    "download_lp" : "git clone https://gitlab.com/liberty-pool/lp-xmrig.git",
    
    "donate_switch_mo" : "cp mobile-miner-termux/noDonation/donate.h xmrig/src/donate.h",
    "donate_switch_lp" : "cp mobile-miner-termux/noDonation/donate.h lp-xmrig/src/donate.h",
    
    "copy_xmrig" : "cp xmrig mobile-miner-termux/download/xmrig/xmrig",
    
    "move_mo" : "mv xmrig mobile-miner-termux/download/mo/xmrig",
    "move_lp" : "mv lp-xmrig mobile-miner-termux/download/lp/lp-xmrig",
    "move_xmrig" : "mv xmrig mobile-miner-termux/download/xmrig/xmrig",
    
    "delete_xmrig" : "rm -rf mobile-miner-termux/download/xmrig/xmrig",
    
    "build_mo" : "mkdir mobile-miner-termux/download/mo/xmrig/build && cd mobile-miner-termux/download/mo/xmrig/build && cmake .. -DWITH_HWLOC=OFF && make",
    "build_lp" : "mkdir mobile-miner-termux/download/lp/lp-xmrig/build && cd mobile-miner-termux/download/lp/lp-xmrig/build && cmake .. -DWITH_HWLOC=OFF && make",
    "build_xmrig" : "mkdir mobile-miner-termux/download/xmrig/xmrig/build && cd mobile-miner-termux/download/xmrig/xmrig/build && cmake .. -DWITH_HWLOC=OFF -DWITH_MO_BENCHMARK=OFF && make",
		
    "get_mo" : "mv mobile-miner-termux/download/mo/xmrig/build/xmrig mobile-miner-termux/rigs/mo && rm -rf mobile-miner-termux/download/mo/xmrig && chmod u+x mobile-miner-termux/rigs/mo",
    "get_lp" : "mv mobile-miner-termux/download/lp/lp-xmrig/build/xmrig mobile-miner-termux/rigs/lp && rm -rf mobile-miner-termux/download/lp/lp-xmrig && chmod u+x mobile-miner-termux/rigs/lp",
    "get_xmrig" : "mv mobile-miner-termux/download/xmrig/xmrig/build/xmrig mobile-miner-termux/rigs/xmrig && rm -rf mobile-miner-termux/download/xmrig/xmrig && chmod u+x mobile-miner-termux/rigs/xmrig"}


# This is list of algos with randomx
AlgosX = [
    "rx/arq",
    "cn/half",
    "cn-heavy/xhv",
    "cn/gpu",
    "cn-pico/trtl",
    "argon2/chukwav2",
    "ghostrider",
    "cn/r",
    "panthera",
    "rx/0"]

# This is list of algos  
Algos = [
	"cn/half",
    "cn-heavy/xhv",
    "cn/gpu",
    "cn-pico/trtl",
    "argon2/chukwav2",
    "ghostrider",
    "cn/r"]
