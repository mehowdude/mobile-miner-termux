# Standard Library
import time

# User-Defined Classes
import Mediator
import DoubleCheck

class Menu(Mediator.ApplicationPart):

	"""
	This class responsible for the Menu of 
	the application

	"""

	RUN_SCRIPT_OPTION = "1"
	CREATE_SCRIPT_OPTION = "2"
	DELETE_SCRIPT_OPTION = "3"
	UPDATE_APP_OPTION = "4"
	UPDATE_MO_OPTION = "5"
	UPDATE_XMRIG_OPTION = "6"
	UPDATE_LP_OPTION = "7"
	UPDATE_ALL_OPTION = "8"

	def Run(self) -> None:
		"""It gets the option from the user
		and notifies appropriate part of the application"""
		
		print("\n++++++++++++++++++++\n")
		print("    Main Menu")
		print("\n++++++++++++++++++++\n")
		print("(1) RunScript")
		print("(2) CreateScript")
		print("(3) DeleteScript")
		print("(4) UpdateApp")
		print("(5) UpdateMO")
		print("(6) UpdateXmrig")
		print("(7) UpdateLP")
		print("(8) UpdateAllMiners")
		print("\n--------------------\n")
		print("(0)exit\n\n")
		
		readyInput = ""
		option = ""
		
		while True:
			readyInput = input("Choose the option: ")
			time.sleep(self.ONE_SEC_DELAY)
			
			if readyInput == self.EXIT_OPTION:
				option = DoubleCheck.AreYouSure()
				if option == True:
					exit()
			
			elif readyInput == self.RUN_SCRIPT_OPTION:
				option = DoubleCheck.AreYouSure()
				if option == True:
					self.mediator.notify("RunScript")
						
			elif readyInput == self.CREATE_SCRIPT_OPTION:
				option = DoubleCheck.AreYouSure()
				if option == True:
					self.mediator.notify("CreateScript")
						
			elif readyInput == self.DELETE_SCRIPT_OPTION:
				option = DoubleCheck.AreYouSure()
				if option == True:
					self.mediator.notify("DeleteScript")
					
			elif readyInput == self.UPDATE_APP_OPTION:
				option = DoubleCheck.AreYouSure()
				if option == True:
					self.mediator.notify("UpdateApp")
						
			elif readyInput == self.UPDATE_MO_OPTION:
				option = DoubleCheck.AreYouSure()
				if option == True:
					self.mediator.notify("UpdateMO")
			
			elif readyInput == self.UPDATE_XMRIG_OPTION:
				option = DoubleCheck.AreYouSure()
				if option == True:
					self.mediator.notify("UpdateXmrig")
						
			elif readyInput == self.UPDATE_LP_OPTION:
				option = DoubleCheck.AreYouSure()
				if option == True:
					self.mediator.notify("UpdateLP")
			
			elif readyInput == self.UPDATE_ALL_OPTION:
				option = DoubleCheck.AreYouSure()
				if option == True:
					self.mediator.notify("UpdateAllMiners")
						
			else:
				print("There is no such option")
