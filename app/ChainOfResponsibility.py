class Handler():

	"""
	This is the Handler interface

	"""

	def SetNext(self, handler):
		pass
	
	
	def Handle(self):
		pass


class AbstractHandler(Handler):
	
	"""
	The class implements default behavior of
	the Handle.

	"""
	ONE_SEC_DELAY = 1
	THREE_SEC_DELAY = 3
	EXIT_OPTION = "0"

	nextHandler = None

	def SetNext(self, handler):
		"""It sets up the next handler
		
		:param Handler handler: it is the next handler
		on the chain
		:returns: the next handler on the chain
		:rtype Handler
		
		"""
		
		#setting up the next handler
		self.nextHandler = handler
		
		# it can be None
		return handler
	
	def Handle(self):
		"""It runs the next handler or exits 
		the chain of responsibility
		
		:returns: the next handler on the chain
		:rtype Handler
		
		"""
		
		# if null then exit the chain otherwise continue with the chain
		if self.nextHandler:
			return self.nextHandler.Handle()
		else:
			return None

