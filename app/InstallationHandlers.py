# Standard Library
import os
import re
import time

# User-Defined Classes
import Dictionary
import ChainOfResponsibility

class InstallationHandler(ChainOfResponsibility.AbstractHandler):

	"""
	This class is initializing first
	installation

	"""
				
	def Handle(self):
		"""It checks if the application was
		already installed, if yes then it will
		exit the chain of responsibility of the
		installation
		
		:returns: the next handler on the chain
		:rtype Handler
		
		"""
		
		# checking if application was installed and getting wallet addres
		# if the application was not installed
		if os.path.exists(Dictionary.Commands.get("mywallet")) == False:
			print("Installation Starts")
			os.system(Dictionary.Commands.get("make_info_dir"))
			os.system(Dictionary.Commands.get("make_script_dir"))
			os.system(Dictionary.Commands.get("make_download_dir"))
			os.system(Dictionary.Commands.get("make_mo_dir"))
			os.system(Dictionary.Commands.get("make_lp_dir"))
			os.system(Dictionary.Commands.get("make_xmrig_dir"))
			os.system(Dictionary.Commands.get("make_rigs_dir"))

			print("Creating a file to store your wallet address")
			wallet = input("copy and paste wallet address here: ")
			time.sleep(self.ONE_SEC_DELAY)
			with open(Dictionary.Commands.get("mywallet"), 'w') as f:
				f.write(wallet)

			# go to next handler
			return super().Handle()
		else:
			
			# exit the chain
			return None


class IntroductionHandler(ChainOfResponsibility.AbstractHandler):
	
	"""
    This class is responsible for displaying the
    first part of the menu
    
    """
    
	def Spaces(self):
		"""It prints enters"""
		
		print("\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n")
		
	def Handle(self):
		"""It prints initial information
		about the application
		
		:returns: the next handler on the chain
		:rtype Handler
		
		"""
		
		self.Spaces()
		print("\n*********************\n")
		print("This is a brief introduction")
		print("to the application\n")
		print("\n*********************\n")
		
		print("\n\n\n\n")
		input("Press Enter to continue...")
		time.sleep(self.ONE_SEC_DELAY)
		
		self.Spaces()
		print("\n*********************\n")
		print("The application allows to create,")
		print("and delete mining scripts\n")
		print("\n*********************\n")
		
		print("\n\n\n\n")
		input("Press Enter to continue...")
		time.sleep(self.ONE_SEC_DELAY)
		
		self.Spaces()
		print("\n*********************\n")
		print("The application allows to update,")
		print("the miners and application itself\n")
		print("\n*********************\n")
		
		print("\n\n\n\n")
		input("Press Enter to continue...")
		time.sleep(self.ONE_SEC_DELAY)
		
		self.Spaces()
		print("\n*********************\n")
		print("Supported Pools")
		print("\n*********************\n")
		print("Liberty-Pool")
		print("MoneroOcean\n")
		print("\n--------------------\n")
		
		print("\n\n\n\n")
		input("Press Enter to continue...")
		time.sleep(self.ONE_SEC_DELAY)
		
		self.Spaces()
		print("\n*********************\n")
		print("Implemented Miners")
		print("\n*********************\n")
		print("Xmrig with all Algos")
		print("MoneroOcean Algo-Switch")
		print("Liberty-Pool Algo-Switch\n")
		print("\n--------------------\n")
		
		print("\n\n\n\n")
		input("Press Enter to continue...")
		time.sleep(self.ONE_SEC_DELAY)
		
		self.Spaces()
		print("\n*********************\n")
		print("Feel free to check the source code on GitLab") 
		print("and to report any bugs the MO or LP discord server") 
		print("If you want to contribute any way, you are also welcome\n")
		print("\n*********************\n")
		
		print("\n\n\n\n")
		input("Press Enter to continue...")
		time.sleep(self.ONE_SEC_DELAY)
		
		self.Spaces()
		
		# go to next handler
		return super().Handle()


class ManualHandler(ChainOfResponsibility.AbstractHandler):
	
	"""
    This class is responsible for displaying the
    manual of the application
    
    """
    
	def Spaces(self):
		"""It prints enters"""
		
		print("\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n")
		
	def Handle(self):
		"""It prints manual of the application
		
		:returns: the next handler on the chain
		:rtype Handler
		
		"""
		
		self.Spaces()
		print("\n*********************\n")
		print("Every creation of mining script")
		print("begins with diving deep into")
		print("your subconsciousness to discover")
		print("what is the I within us")
		print("and later becoming enlighten")
		print("by taking long spiritual journey")
		print("which takes many long years")
		print("\n*********************\n")
		print("\n\n\n\n")
		input("Press Enter to continue...")
		time.sleep(self.ONE_SEC_DELAY)
		
		self.Spaces()
		print("\n*********************\n")
		print("thus, creating a simple mining script")
		print("could take decates, or even multiple rebirths")
		print("However, this application allows you to create a mining")
		print("script within one minute which is simply unprecedented.")
		print("This application was created with the direct cooperation of")
		print("greatest minds of our generation such as Archimedes of Syracuse")
		print("Leonardo da Vinci, Nicolaus Copernicusm, Albert Einstein")
		print("Stephen Hawking, Dalai Lama and other very famous people\n")
		print("\n*********************\n")
		print("\n\n\n\n")
		input("Press Enter to continue...")
		time.sleep(self.ONE_SEC_DELAY)
		
		self.Spaces()
		print("\n*********************\n")
		print("The menu of the application has")
		print("multiple choices such as: ")
		print("1. Updating the miners and application itself")
		print("2. Creating multiple mining scripts")
		print("3. Deleting existing mining scripts")
		print("4. Running existing mining scripts")
		print("\n*********************\n")
		
		print("\n\n\n\n")
		input("Press Enter to continue...")
		time.sleep(self.ONE_SEC_DELAY)
		
		self.Spaces()
		print("\n*********************\n")
		print("    	  Updating")
		print("\n*********************\n")
		print("The application allows to")
		print("choose a miner or application\n")
		print("\n--------------------\n")
		
		print("\n\n\n\n")
		input("Press Enter to continue...")
		time.sleep(self.ONE_SEC_DELAY)
		
		self.Spaces()
		print("\n*********************\n")
		print("  Creating the Scripts")
		print("\n*********************\n")
		print("Creation process has multiple stages")
		print("which simplify creation of a mining script\n")
		print("\n--------------------\n")
		
		print("\n\n\n\n")
		input("Press Enter to continue...")
		time.sleep(self.ONE_SEC_DELAY)
		
		self.Spaces()
		print("\n*********************\n")
		print("  Deleting the Scripts")
		print("\n*********************\n")
		print("Involves choosing a script from the list")
		print("and simply deleting it from the list\n")
		print("\n--------------------\n")
		
		print("\n\n\n\n")
		input("Press Enter to continue...")
		time.sleep(self.ONE_SEC_DELAY)
		
		self.Spaces()
		print("\n*********************\n")
		print("  Running the Scripts")
		print("\n*********************\n")
		print("Involves choosing a script from the list")
		print("and simply running it through the application\n")
		print("\n--------------------\n")
		
		print("\n\n\n\n")
		input("Press Enter to continue...")
		time.sleep(self.ONE_SEC_DELAY)
		
		self.Spaces()
		
		# go to next handler
		return super().Handle()

        
class MOInstalliationHandler(ChainOfResponsibility.AbstractHandler):

	"""
	This class is initializing the installment 
	of the MoneroOcean algo-switch 

	"""
				
	def Handle(self):
		"""it invokes all the commands responsible for the
		installation of the miner
		
		"""
		
		print("\n\n\n\n")
		print("Installing MoneroOcean Algo-Switch")
		print("This takes a long time")
		print("Donation are going to be set to 0 automatically")
		time.sleep(self.THREE_SEC_DELAY)
		
		#run the mo installation
		os.system(Dictionary.Commands.get("download_mo"))
		os.system(Dictionary.Commands.get("donate_switch_mo"))
		os.system(Dictionary.Commands.get("copy_xmrig"))
		os.system(Dictionary.Commands.get("move_mo"))
		os.system(Dictionary.Commands.get("build_mo"))
		os.system(Dictionary.Commands.get("get_mo"))
		
		print("\n\n\n\n")
		print("Completed!!!")
		time.sleep(self.THREE_SEC_DELAY)
		
		# go to next handler
		return super().Handle()


class XmrigInstalliationHandler(ChainOfResponsibility.AbstractHandler):

	"""
	This class is initializing the installment 
	of xmrig with all the algos 

	"""
				
	def Handle(self):
		"""it invokes all the commands responsible for the
		installation of the miner
		
		:returns: the next handler on the chain
		:rtype Handler
		
		"""
		
		print("\n\n\n\n")
		print("Installing Xmrig with all the Algos")
		print("This takes a long time")
		print("Donation are going to be set to 0 automatically")
		time.sleep(self.THREE_SEC_DELAY)
		
		# if only updating xmrig
		if os.path.exists(Dictionary.Commands.get("install_xmrig")) == False:
		
			os.system(Dictionary.Commands.get("download_mo"))
			os.system(Dictionary.Commands.get("donate_switch_mo"))
			os.system(Dictionary.Commands.get("move_xmrig"))
		
		#run the xmrig installation	
		os.system(Dictionary.Commands.get("build_xmrig"))
		os.system(Dictionary.Commands.get("get_xmrig"))
		
		print("\n\n\n\n")
		print("Completed!!!")
		time.sleep(self.THREE_SEC_DELAY)
		
		# go to next handler
		return super().Handle()

		
class LPInstalliationHandler(ChainOfResponsibility.AbstractHandler):
	
	"""
	This class is initializing the installment 
	of the Liberty-Pool algo-switch 

	"""
				
	def Handle(self):
		"""it invokes all the commands responsible for the
		installation of the miner
		
		:returns: the next handler on the chain
		:rtype Handler
		
		"""
		
		print("\n\n\n\n")
		print("Installing Liberty-Pool Algo-Switch")
		print("This takes a long time")
		print("Donation are going to be set to 0 automatically")
		time.sleep(self.THREE_SEC_DELAY)
		
		#run the lp installation
		os.system(Dictionary.Commands.get("download_lp"))
		os.system(Dictionary.Commands.get("donate_switch_lp"))
		os.system(Dictionary.Commands.get("move_lp"))
		os.system(Dictionary.Commands.get("build_lp"))
		os.system(Dictionary.Commands.get("get_lp"))
		
		print("\n\n\n\n")
		print("Completed!!!")
		time.sleep(self.THREE_SEC_DELAY)
		
		# go to next handler
		return super().Handle()
