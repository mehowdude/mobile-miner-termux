# Standard Library
import time

def AreYouSure(ONE_SEC_DELAY = 1) -> bool:
	"""It makes sure the input was intended

	:return: the choice of the user
	:rtype: bool
	"""

	sure = ""

	while True:
		sure = input("Are you sure of the choice? (Y)es/(N)o: ")
		time.sleep(ONE_SEC_DELAY)

		if sure.lower() == "yes" or sure.lower() == "y":
			return True

		elif sure.lower() == "no" or sure.lower() == "n":
			return False

		else:
			print("There is no such option")
