#!/bin/sh 

echo "Installing the Application" &&
echo "Only the Application files are updated" &&
sleep 5 &&

cd mobile-miner-termux/updater &&
git clone https://gitlab.com/mehowdude/mobile-miner-termux.git &&
rm -rf ../app &&
rm ../../start.sh &&
chmod -R u+x mobile-miner-termux &&
mv mobile-miner-termux/app ../app &&
mv mobile-miner-termux/start.sh ../../start.sh &&
mv mobile-miner-termux/updater/updater.sh updater.sh &&
rm -rf mobile-miner-termux &&
cd &&

echo "\n\n\n\n" &&
echo "Installing is Completed!!!" &&
echo "Run the start.sh to start the Application" &&
rm mobile-miner-termux/updater/old_updater.sh
